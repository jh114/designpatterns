package observer;

public abstract class Observer {
	protected Observable observable;
	
	public Observer(Observable observable) {
		this.observable = observable;
	}
	
	/*
	 * This method is to be overloaded by the implementation.
	 * It is called whenever the Observables data changes.
	 * To use the pull method use the "observable" variable.
	 * */
	public abstract void obsNotify();
}