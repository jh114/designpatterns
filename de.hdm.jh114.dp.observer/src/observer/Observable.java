package observer;

import java.util.ArrayList;

public abstract class Observable {
	protected ArrayList<Observer> observers;
	
	public Observable() {
		this.observers = new ArrayList<Observer>();
	}
	
	public void addObserver(Observer observer) {
		this.observers.add(observer);
	}
	
	public void removeObserver(Observer observer) {
		this.observers.remove(observer);
	}
	
	protected void notifyObservers() {
		for(int i = 0; i < this.observers.size(); i++) {
			this.observers.get(i).obsNotify();
		}
	}
}