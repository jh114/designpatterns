package application;

import observer.Observable;

public class ListMonitor extends Monitor {
	public ListMonitor(Observable observable) {
		super(observable);
	}
	
	public void display() {
		//do stuff to display actual data
		System.out.println("Listy saez: " + this.cpuLoad +  " - " + this.ramUsage + " - " + this.lan1Load + " - " + this.lan2Load);
	}
}
