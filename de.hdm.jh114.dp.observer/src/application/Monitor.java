package application;

import observer.Observable;
import observer.Observer;

public abstract class Monitor extends Observer {
	protected double cpuLoad;
	protected double ramUsage;
	protected double lan1Load;
	protected double lan2Load;
	
	public Monitor(Observable observable) {
		super(observable);
	}
	
	public void obsNotify() {
		/*handle new data here (pull)*/
		ResourceManager resManager = (ResourceManager)this.observable;
		this.cpuLoad = resManager.getCpuLoad();
		this.ramUsage = resManager.getRamUsage();
		this.lan1Load = resManager.getLan1Load();
		this.lan2Load = resManager.getLan2Load();
		
		this.display();
	}
	
	/*To be implemented by child class.*/
	protected abstract void display();
}
