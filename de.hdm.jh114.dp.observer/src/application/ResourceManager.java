package application;

import observer.Observable;

public class ResourceManager extends Observable {
	private double cpuLoad;
	private double ramUsage;
	private double lan1Load;
	private double lan2Load;
	
	public double getCpuLoad() {
		return this.cpuLoad;
	}
	
	public double getRamUsage() {
		return this.ramUsage;
	}
	
	public double getLan1Load() {
		return this.lan1Load;
	}
	
	public double getLan2Load() {
		return this.lan2Load;
	}
	
	public void setCpuLoad(double cpuLoad) {
		this.cpuLoad = cpuLoad;
		this.notifyObservers();
	}

	public void setRamUsage(double ramUsage) {
		this.ramUsage = ramUsage;
		this.notifyObservers();
	}

	public void setLan1Load(double lan1Load) {
		this.lan1Load = lan1Load;
		this.notifyObservers();
	}

	public void setLan2Load(double lan2Load) {
		this.lan2Load = lan2Load;
		this.notifyObservers();
	}
}
