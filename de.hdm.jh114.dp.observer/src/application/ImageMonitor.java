package application;

import observer.Observable;

public class ImageMonitor extends Monitor {
	public ImageMonitor(Observable observable) {
		super(observable);
	}
	
	public void display() {
		//do stuff to display actual data
		System.out.println("Imgy saez: " + this.cpuLoad +  " - " + this.ramUsage + " - " + this.lan1Load + " - " + this.lan2Load);
	}
}
