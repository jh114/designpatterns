package test;

import application.GraphMonitor;
import application.ImageMonitor;
import application.ListMonitor;
import application.ResourceManager;

public class Test {
	public static void main(String[] args) {
		/*remote system (init)*/
		ResourceManager resManager = new ResourceManager();
		resManager.setCpuLoad(23.3);
		resManager.setRamUsage(85.2);
		resManager.setLan1Load(0.0);
		resManager.setLan2Load(0.0);
		
		/*a client (one monitor view)*/
		ListMonitor listMon = new ListMonitor(resManager);
		resManager.addObserver(listMon);
		
		/*next client (two monitor views)*/
		GraphMonitor graphMon = new GraphMonitor(resManager);
		ImageMonitor imgMon = new ImageMonitor(resManager);
		resManager.addObserver(graphMon);
		resManager.addObserver(imgMon);
		
//		/*remote system (data changes due to network activity)*/
//		resManager.setLan2Load(100.0);
//		
//		/*second client now only with one monitor view*/
//		resManager.removeObserver(imgMon);
//		
//		System.out.println();		
//		
//		/*remote system (another data change)*/
//		resManager.setCpuLoad(2.1);
//	
//		System.out.println();
//		
//		resManager.setLan2Load(7.5);
	}
}